public class Percolation {
    private int size;
    private int virtualTop;
    private int virtualBottom;
    private int notAPoint;
    private WeightedQuickUnionUF points;
    private boolean[] openStatus;

    public Percolation(int N) {
        size = N;
        int numberOfPoints = N * N;

        int virtualPoints = 3;
        virtualTop = numberOfPoints; // position
        virtualBottom = numberOfPoints + 1; // position
        notAPoint = numberOfPoints + 2; // yes, this is used like a point

        points = new WeightedQuickUnionUF(numberOfPoints + virtualPoints);
        openStatus = new boolean[numberOfPoints + virtualPoints];

        openStatus[virtualTop] = true;
        openStatus[virtualBottom] = true;
    }

    public void open(int i, int j) {
        validateIndex(i, j);
        int currentPoint = index2point(i, j);
        if (openStatus[currentPoint]) return;
        openStatus[currentPoint] = true;

        int leftPoint = index2point(i, j - 1);
        int rightPoint = index2point(i, j + 1);
        int abovePoint = index2point(i - 1, j);
        int belowPoint = index2point(i + 1, j);

        connectIfOpen(currentPoint, leftPoint);
        connectIfOpen(currentPoint, rightPoint);
        connectIfOpen(currentPoint, abovePoint);
        connectIfOpen(currentPoint, belowPoint);
    }

    public boolean isOpen(int i, int j) {
        validateIndex(i, j);
        int point = index2point(i, j);
        return openStatus[point];
    }

    public boolean isFull(int i, int j) {
        validateIndex(i, j);
        int point = index2point(i, j);
        return points.connected(point, virtualTop);
    }

    public boolean percolates() {
        return points.connected(virtualBottom, virtualTop);
    }

    private void validateIndex(int i, int j) {
        if (i < 1 || i > size || j < 1 || j > size) {
            throw new IndexOutOfBoundsException("Mine!");
        }
    }

    private int index2point(int i, int j) {
        if (i == 0) {
            return virtualTop;
        } else if (i == size + 1) {
            return virtualBottom;
        } else if (j < 1 || j > size) {
            return notAPoint;
        }
        return (i - 1) * size + j - 1;
    }

    private void connectIfOpen(int currentPoint, int otherPoint) {
        if (openStatus[otherPoint]) points.union(currentPoint, otherPoint);
    }

    public static void main(String[] args) {
        Percolation test = new Percolation(4);
        StdOut.println(test.points.connected(0, 1) + " (should be false)");
        test.open(1, 1);
        test.open(4, 4);
        StdOut.println(test.points.connected(0, 1) + " (should be false)");
        test.open(1, 2);
        StdOut.println(test.points.connected(0, 1) + " (should be true)");
    }
}
