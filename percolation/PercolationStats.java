public class PercolationStats {
    private double[] stats;

    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) throw new IllegalArgumentException("Mine");

        //Stopwatch timer = new Stopwatch();
        stats = new double[T];
        Percolation perc;

        int gridSize = N;
        for (int i = 0; i < T; i++) {
            perc = new Percolation(N);
            stats[i] = threshold(perc, gridSize);
        }
        //StdOut.println("execution time          = "
        //        + timer.elapsedTime() + " seconds");
    }

    private static double threshold(Percolation perc, int gridSize) {
        double openSites = 0;
        while (!perc.percolates()) {
            int i = StdRandom.uniform(1, gridSize + 1);
            int j = StdRandom.uniform(1, gridSize + 1);
            if (!perc.isOpen(i, j)) {
                perc.open(i, j);
                openSites++;
            }
        }
        return (double) openSites / (gridSize * gridSize);
    }

    public double mean() {
        return StdStats.mean(stats);
    }

    public double stddev() {
        return StdStats.stddev(stats);
    }

    public double confidenceLo() {
        return mean() - 1.96 * stddev() / Math.sqrt(stats.length);
    }

    public double confidenceHi() {
        return mean() + 1.96 * stddev() / Math.sqrt(stats.length);
    }

    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats testCase = new PercolationStats(N, T);

        StdOut.println("mean                    = " + testCase.mean());
        StdOut.println("stddev                  = " + testCase.stddev());
        StdOut.println("95% confidence interval = "
                + testCase.confidenceLo() + ", " + testCase.confidenceHi());
    }
}
