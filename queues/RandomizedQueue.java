import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] data;
    private int size;

    public RandomizedQueue() {
        data = (Item[]) new Object[1];
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void enqueue(Item item) {
        if (item == null) throw new NullPointerException("Mine");
        if (size == data.length) data = resized(data, 2 * data.length);

        int index = StdRandom.uniform(0, size + 1);
        data[size++] = data[index];
        data[index] = item;
    }

    public Item dequeue() {
        if (size == 0) throw new NoSuchElementException("Mine");

        int index = StdRandom.uniform(0, size);
        Item ret = data[index];
        data[index] = data[--size];
        data[size] = null;

        if (size > 1 && size < data.length / 4)
            data = resized(data, data.length / 2);
        return ret;
    }

    public Item sample() {
        if (size == 0) throw new NoSuchElementException("Mine");

        int index = StdRandom.uniform(0, size);
        return data[index];
    }

    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private Item[] randomData;
        private int itemsLeft;

        public RandomizedQueueIterator() {
            randomData = resized(data, size);
            StdRandom.shuffle(randomData);
            itemsLeft = size;
        }
        
        public boolean hasNext() {
            return itemsLeft > 0;
        }
        
        public Item next() {
            if (itemsLeft < 1) throw new NoSuchElementException("Mine");

            return randomData[--itemsLeft];
        }
        
        public void remove() {
            throw new UnsupportedOperationException("Mine");
        }
    }

    private Item[] resized(Item[] list, int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        int j = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) copy[j++] = list[i];
        }
        return copy;
    }

    public static void main(String[] args) {
        RandomizedQueue<Integer> r = new RandomizedQueue<Integer>();
        StdOut.println("empty = " + r.isEmpty());

        StdOut.println("adding...");
        for (int i = 0; i < 10; i++) r.enqueue(i);
        StdOut.println("size = " + r.size());

        StdOut.print("contents 1: ");
        for (int i : r) StdOut.print(i + " ");

        StdOut.print("\ncontents 2: ");
        for (int i : r) StdOut.print(i + " "); 

        StdOut.print("\ncontents 3: ");
        for (int i : r) StdOut.print(i + " ");
        StdOut.println("\nempty = " + r.isEmpty()); 

        StdOut.println("sample 1: " + r.sample());
        StdOut.println("sample 2: " + r.sample());
        StdOut.println("sample 3: " + r.sample());

        StdOut.print("removing: ");
        for (int i = 0; i < 10; i++) StdOut.print(r.dequeue() + " ");
        StdOut.println("\nsize = " + r.size());
        StdOut.println("empty = " + r.isEmpty());
    }
}
