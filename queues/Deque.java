import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private Node first;
    private Node last;
    private int size;

    private class Node {
        Node prev;
        Node next;
        Item item;
    }

    public Deque() {
        first = null;
        last = null;
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        if (item == null) throw new NullPointerException("Mine");

        Node n = new Node();
        n.item = item;
        n.prev = null;

        if (first == null) {
            n.next = null;
            last = n;
        } else {
            n.next = first;
            first.prev = n;
        }

        first = n;
        size++;
    }

    public void addLast(Item item) {
        if (item == null) throw new NullPointerException("Mine");

        Node n = new Node();
        n.item = item;
        n.next = null;

        if (last == null) {
            n.prev = null;
            first = n;
        } else {
            n.prev = last;
            last.next = n;
        }

        last = n;
        size++;
    }

    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("Mine");

        Item ret = first.item;

        if (size > 1) {
            first = first.next;
            first.prev = null;
        } else first = last = null;

        size--;
        return ret;
    }

    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException("Mine");

        Item ret = last.item;

        if (size > 1) {
            last = last.prev;
            last.next = null;
        } else first = last = null;

        size--;
        return ret;
    }

    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException("Mine");
        }

        public Item next() {
            if (current == null) throw new NoSuchElementException("Mine");

            Item ret = current.item;
            current = current.next;
            return ret;
        }
    }

    public static void main(String[] args) {
        Deque<Integer> d = new Deque<Integer>();
        StdOut.println("empty   = " + d.isEmpty());

        StdOut.println("adding...");
        for (int i = 4; i >= 0; i--) d.addFirst(i);
        StdOut.println("size    = " + d.size());

        StdOut.println("adding...");
        for (int i = 5; i <= 9; i++) d.addLast(i);
        StdOut.println("size    = " + d.size());

        StdOut.print("contense: ");
        for (int i : d) StdOut.print(i + " ");
        StdOut.println("\nempty   = " + d.isEmpty());

        StdOut.print("removing: ");
        for (int i = 5; i <= 9; i++) StdOut.print(d.removeLast() + " ");
        StdOut.println("\nsize    = " + d.size());

        StdOut.print("removing: ");
        for (int i = 4; i >= 0; i--) StdOut.print(d.removeFirst() + " ");
        StdOut.println("\nsize    = " + d.size());

        StdOut.print("contense: ");
        for (int i : d) StdOut.print(i + " ");
        StdOut.println("\nempty   = " + d.isEmpty());
    }
}
