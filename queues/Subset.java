public class Subset {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        RandomizedQueue<String> subset = new RandomizedQueue<String>();
        String[] strings = StdIn.readStrings();

        for (String s : strings) subset.enqueue(s);
        for (int i = 0; i < k; i++) StdOut.println(subset.dequeue());
    }
}
