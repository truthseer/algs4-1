import java.util.Arrays;

public class Fast {
    public static void main(String[] args) {
        int[] ints = In.readInts(args[0]);
        Point[] points = new Point[ints[0]];

        for (int i = 1, j = 0; i < 2 * points.length; j++)
            points[j] = new Point(ints[i++], ints[i++]);
        Arrays.sort(points);

        Point[] ordered = points.clone();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
            Arrays.sort(ordered, p.SLOPE_ORDER);

            for (int i = 1; i < ordered.length; i++) {
                int j;
                double slope = p.slopeTo(ordered[i]);
                for (j = i + 1; j < ordered.length; j++) {
                    if (p.slopeTo(ordered[j]) != slope) {
                        //j--;
                        break;
                    }
                }

                int k = j - i;
                if (k >= 3) {
                    Point[] pts = new Point[k + 1];
                    System.arraycopy(ordered, i, pts, 0, k);
                    pts[k] = p;
                    Arrays.sort(pts);

                    StdOut.print(pts[0]);
                    for (int l = 1; l < k + 1; l++) {
                        StdOut.print(" -> " + pts[l]);
                    }
                    StdOut.print("\n");
                    pts[0].drawTo(pts[k]);
                    i += k;
                }
            }
            Point[] orderedTemp = new Point[ordered.length - 1];
            System.arraycopy(
                    ordered, 1, orderedTemp, 0, ordered.length - 1);
            ordered = orderedTemp;
        }
    }
}
