import java.util.Arrays;

public class Brute {
    public static void main(String[] args) {
        int[] ints = In.readInts(args[0]);
        Point[] points = new Point[ints[0]];

        for (int i = 1, j = 0; i < 2 * points.length; j++)
            points[j] = new Point(ints[i++], ints[i++]);
        Arrays.sort(points);

        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (int i = 0; i < points.length; i++) {
            points[i].draw();
            for (int j = i + 1; j < points.length; j++) {
                for (int k = j + 1; k < points.length; k++) {
                    for (int l = k + 1; l < points.length; l++) {
                        if (    points[i].slopeTo(points[j]) ==
                                points[i].slopeTo(points[k]) &&
                                points[i].slopeTo(points[j]) ==
                                points[i].slopeTo(points[l])) {
                            StdOut.println(points[i] + " -> "
                                    + points[j] + " -> "
                                    + points[k] + " -> "
                                    + points[l]);
                            points[i].drawTo(points[l]);
                        }
                    }
                }
            }
        }
    }
}
