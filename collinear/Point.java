import java.util.Comparator;
import java.util.Arrays;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new BySlope();

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    private class BySlope implements Comparator<Point> {
        public int compare(Point v, Point w) {
            double slope1 = slopeTo(v);
            double slope2 = slopeTo(w);
            if (slope1 < slope2) return -1;
            else if (slope1 == slope2) return 0;
            else return 1;
        }
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        double dx = that.x - x;
        double dy = that.y - y;

        if (dx == 0 && dy == 0) return Double.NEGATIVE_INFINITY;
        else if (dy == 0) return 0;
        else if (dx == 0) return Double.POSITIVE_INFINITY;
        else return dy / dx;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
        if (y < that.y) return -1;
        else if (y == that.y) {
            if (x < that.x) return -1;
            else if (x == that.x) return 0;
            else return 1;
        } else return 1;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        Point p1 = new Point(1, 1);
        Point p2 = new Point(8, 9);
        Point p3 = new Point(8, 8);
        Point p4 = new Point(9, 9);
        Point[] points = {p1, p2, p3, p4};

        StdOut.println("p1.compareTo(p2) = " + p1.compareTo(p2));
        StdOut.println("p1.compareTo(p3) = " + p1.compareTo(p3));
        StdOut.println("p1.compareTo(p4) = " + p1.compareTo(p4));
        StdOut.println("p2.compareTo(p3) = " + p2.compareTo(p3));
        StdOut.println("p2.compareTo(p4) = " + p2.compareTo(p4));
        StdOut.println("p3.compareTo(p4) = " + p3.compareTo(p4));

        StdOut.println("\np1.slopeTo(p1) = " + p1.slopeTo(p1));
        StdOut.println("p1.slopeTo(p2) = " + p1.slopeTo(p2));
        StdOut.println("p1.slopeTo(p3) = " + p1.slopeTo(p3));
        StdOut.println("p1.slopeTo(p4) = " + p1.slopeTo(p4));
        StdOut.println("\np2.slopeTo(p1) = " + p2.slopeTo(p1));
        StdOut.println("p2.slopeTo(p2) = " + p2.slopeTo(p2));
        StdOut.println("p2.slopeTo(p3) = " + p2.slopeTo(p3));
        StdOut.println("p2.slopeTo(p4) = " + p2.slopeTo(p4));
        StdOut.println("\np3.slopeTo(p1) = " + p3.slopeTo(p1));
        StdOut.println("p3.slopeTo(p2) = " + p3.slopeTo(p2));
        StdOut.println("p3.slopeTo(p3) = " + p3.slopeTo(p3));
        StdOut.println("p3.slopeTo(p4) = " + p3.slopeTo(p4));
        StdOut.println("\np4.slopeTo(p1) = " + p4.slopeTo(p1));
        StdOut.println("p4.slopeTo(p2) = " + p4.slopeTo(p2));
        StdOut.println("p4.slopeTo(p3) = " + p4.slopeTo(p3));
        StdOut.println("p4.slopeTo(p4) = " + p4.slopeTo(p4));

        StdOut.print("\nnonsorted: ");
        for (Point point : points) StdOut.print(point);

        Arrays.sort(points, p1.SLOPE_ORDER);
        StdOut.print("\np1 sorted: ");
        for (Point point : points) StdOut.print(point);

        Arrays.sort(points, p2.SLOPE_ORDER);
        StdOut.print("\np2 sorted: ");
        for (Point point : points) StdOut.print(point);

        Arrays.sort(points, p3.SLOPE_ORDER);
        StdOut.print("\np3 sorted: ");
        for (Point point : points) StdOut.print(point);

        Arrays.sort(points, p4.SLOPE_ORDER);
        StdOut.print("\np4 sorted: ");
        for (Point point : points) StdOut.print(point);
        StdOut.print("\n");
    }
}
